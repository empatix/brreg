<?php

namespace Empatix;

use Exception;
use GuzzleHttp\Client;
use Empatix\Exceptions\BrregException;

class Brreg
{
	static $baseUrl = 'https://data.brreg.no/enhetsregisteret/api/enheter';

	public static function get($identifier)
	{
		if (is_numeric($identifier) && strlen($identifier) != 9) {
			throw new BrregException('Organization number must be nine digits');
		}

		if ( ! is_numeric($identifier) && strlen($identifier) === 0)  {
			throw new BrregException('Organization name must be at least 1 letter');
		}

		try {
			return static::response((new Client)->get(static::url($identifier)));
		} catch(Exception $e) {
			throw new BrregException($e->getMessage());
		}
	}

	public static function isValidOrganizationNumber($number)
	{
		$company = static::response((new Client)->get(static::url($number)));

		return $company != null && count($company) !== 0;
	}

	private static function url($identifier)
	{
		if (static::identifierIsOrganizationNumber($identifier)) {
			return sprintf('%s/%s?', static::$baseUrl, $identifier);
		}

		return sprintf('%s/?navn=%s&', static::$baseUrl, urlencode($identifier));
	}
    
    protected static function identifierIsOrganizationNumber($identifier)
    {
        return preg_match('/^\d{9}$/', strval($identifier)) === 1;
    }

	private static function response($response)
	{
		return json_decode((string) $response->getBody(), true);
	}
}
