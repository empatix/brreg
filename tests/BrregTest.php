<?php

namespace Tests;

use Empatix\Brreg;
use PHPUnit\Framework\TestCase;
use Empatix\Exceptions\BrregException;

class BrregTest extends TestCase
{
	/** @test */
	public function throws_exception_if_organization_number_is_to_short()
	{
		try {
			Brreg::get('12345');
		} catch (BrregException $e) {
			$this->assertEquals($e->getMessage(), 'Organization number must be nine digits');
			return;
		}

		$this->fail('Should Not Have Passed');
	}

	/** @test */
	public function throws_exception_if_organization_number_is_to_long()
	{
		try {
			Brreg::get('1234567890');
		} catch (BrregException $e) {
			$this->assertEquals($e->getMessage(), 'Organization number must be nine digits');
			return;
		}

		$this->fail('Should Not Have Passed');
	}

	/** @test */
	public function throws_exception_if_organization_is_not_found()
	{
		try {
			Brreg::get('123456789');
		} catch (BrregException $e) {
			return;
		}

		$this->fail('Should Not Have Passed');
	}

	/** @test */
	public function can_get_organization_by_organization_number()
	{
		$response = Brreg::get('999183719');

		$this->assertTrue($response['navn'] == 'PINDENA AS');
	}

	/** @test */
	public function can_get_organization_list_that_matches_partial_input()
	{
		$response = Brreg::get('pind');

		$this->assertTrue(count($response) > 0);
	}
	
	/** @test */
	public function properly_evaluates_the_organisation_identifier()
	{
        $response = Brreg::get('987895993');
        
        $this->assertTrue(count($response) > 0);
	}
	
}
